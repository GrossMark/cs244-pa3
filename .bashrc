export PS1='\h:\w\$ '
umask 022

alias ls='ls --color=auto'
alias l='cd /usr/src/linux-$(uname -r)'
alias q='cd vimal/queue'
alias w="watch -n 1"
alias r="cd vimal/queue/rcp"
source /etc/bash_completion

PATH=/root/bin:/usr/src/linux-2.6.37.2/tools/perf:$PATH

txtblk='\[\e[0;30m\]' # Black - Regular
txtred='\[\e[0;31m\]' # Red
txtgrn='\[\e[0;32m\]' # Green
txtylw='\[\e[0;33m\]' # Yellow
txtblu='\[\e[0;34m\]' # Blue
txtpur='\[\e[0;35m\]' # Purple
txtcyn='\[\e[0;36m\]' # Cyan
txtwht='\[\e[0;37m\]' # White
bldblk='\[\e[1;30m\]' # Black - Bold
bldred='\[\e[1;31m\]' # Red
bldgrn='\[\e[1;32m\]' # Green
bldylw='\[\e[1;33m\]' # Yellow
bldblu='\[\e[1;34m\]' # Blue
bldpur='\[\e[1;35m\]' # Purple
bldcyn='\[\e[1;36m\]' # Cyan
bldwht='\[\e[1;37m\]' # White
unkblk='\[\e[4;30m\]' # Black - Underline
undred='\[\e[4;31m\]' # Red
undgrn='\[\e[4;32m\]' # Green
undylw='\[\e[4;33m\]' # Yellow
undblu='\[\e[4;34m\]' # Blue
undpur='\[\e[4;35m\]' # Purple
undcyn='\[\e[4;36m\]' # Cyan
undwht='\[\e[4;37m\]' # White
bakblk='\[\e[40m\]'   # Black - Background
bakred='\[\e[41m\]'   # Red
badgrn='\[\e[42m\]'   # Green
bakylw='\[\e[43m\]'   # Yellow
bakblu='\[\e[44m\]'   # Blue
bakpur='\[\e[45m\]'   # Purple
bakcyn='\[\e[46m\]'   # Cyan
bakwht='\[\e[47m\]'   # White
txtrst='\[\e[0m\]'    # Text Reset

PS1="${txtcyn}\h:${txtpur}\w⚡${txtrst} "
#bind -f /root/.bash_key_bindings

alias tstats='watch -n 1 "echo 1 > /proc/timer_stats; sleep 1; head -n 20 /proc/timer_stats; echo ----; tail -n 5 /proc/timer_stats;  echo 0 > /proc/timer_stats"'

export PATH=$PATH:/usr/local/hadoop/bin
export JAVA_HOME=/usr/lib/jvm/java-6-sun
function serve {
  port="${1:-3000}"
  ruby -r webrick -e "s = WEBrick::HTTPServer.new(:Port => $port, :DocumentRoot => Dir.pwd); trap('INT') { s.shutdown }; s.start"
}

who

#LD_LIBRARY_PATH
OTCL_LIB=/home/ubuntu/ns-allinone-2.35/otcl-1.14/
NS2_LIB=/home/ubuntu/ns-allinone-2.35/lib/
USR_Local_LIB=/usr/local/lib/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OTCL_LIB:$NS2_LIB:$USR_Local_LIB

#TCL_LIBRARY
TCL_LIB=/home/ubuntu/ns-allinone-2.35/tcl8.5.10/library/
USR_LIB=/usr/lib/
export TCL_LIBRARY=$TCL_LIBRARY:$TCL_LIB:$USR_LIB

#PATH

XGRAPH=/home/ubuntu/ns-allinone-2.35/xgraph-12.2/:/home/ubuntu/ns-allinone-2.35/bin/:/home/ubuntu/ns-allinone-2.35/tcl8.5.10/unix/:/home/ubuntu/ns-allinone-2.35/tk8.5.10/unix/

NS=/home/ubuntu/ns-allinone-2.35/ns-2.35/
NAM=/home/ubuntu/ns-allinone-2.35/nam-1.15/
export PATH=$PATH:$XGRAPH:$NS:$NAM
