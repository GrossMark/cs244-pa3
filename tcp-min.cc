#ifndef lint
static const char rcsid[] = 
	"@(#) $Header: /cvsroot/nsnam/ns-2/tcp/tcp-min.cc, v 1.00 2015/05/26 18:00:00 mgaf Exp $ (LBL)";
#endif

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "ip.h"
#include "tcp.h"
#include "flags.h"

static class MinTcpClass : public TclClass {
public:
	MinTcpClass() : TclClass("Agent/TCP/MinTcp") {}
	TclObject* create(int, const char*const*) {
		return (new MinTcpAgent());
	}

} class_mintcp;


