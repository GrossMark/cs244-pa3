from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('--infile', '-if',
	            dest="in_file",
		    type=str,
		    action='store',
		    required=True)
parser.add_argument('--workload', '-wl',
	            dest="wkld",
		    type=float,
		    action='store',
		    required=True)
parser.add_argument('--cdf', '-c',
	            dest="cdf_type",
		    type=str,
		    action='store',
		    required=True)
args = parser.parse_args()
with open(args.in_file, 'r') as f:
    lines = f.readlines()
print "finished reading file...parsing...\n"
opened_files = {};
for l in lines:
    trace_fid = int(l.split(" ")[7])
    
    filename = "parsed_flows/" + args.cdf_type + "/" + str(args.wkld) + "/" + "flow_" + str(trace_fid) + ".txt"
   # files.append(filename)
    if opened_files.has_key(filename) == False:
	#files.append(filename)
	new_open = open(filename, 'a')
	opened_files[filename]=new_open

    flw_line = l
    opened_files[filename].write(flw_line)
print "finished parsing files....closing old files..."
for fo in opened_files.values():
    fo.close()
print "finished parsing..."
   # with open(filename, 'a') as flw:
#	flw_line = l
#	flw.write(flw_line)
    

	


    

