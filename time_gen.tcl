#!/usr/local/bin/tclsh
if {$argc != 3} {
  puts stderr "$argv0: Usage: ns $argv0 <workload_cdf> <workload> <# times to
  generate>"
  exit 1
}
set  cdf_type [lindex $argv 0]
set wl [lindex $argv 1]
set numtimes [lindex $argv 2]
set pktsize [expr {1500 * 8}]
set avg_flow_size 9700
set link_rate 1000000000
#TODO: 54->16!!!
set edge_nodes 16
if {$cdf_type == "nw"} {
  set avg_flow_size 1000
  #TODO find avg_flow size for other cdf
}
set times [open "times/$cdf_type/times_$wl.txt" "w"]
set avg_time_interval [expr {($avg_flow_size * $pktsize)/($link_rate *
    $edge_nodes * $wl)}]
set start_time 0.0
set rdm [new RandomVariable/Exponential]
$rdm set avg_ $avg_time_interval
set separator ""
for {set i 0} {$i < $numtimes} {incr i} {
  set t_int [$rdm value]
  set start_time [expr {$start_time + $t_int}]
  puts -nonewline $times $separator$start_time
  set separator "\n"

}
close $times

