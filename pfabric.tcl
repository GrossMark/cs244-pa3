#arguments into simulation
if {$argc != 3} {
  puts stderr "$argv0: Usage: ns $argv0 <wk_load_cdf> <workload>"
  exit 1
}
set workload [lindex $argv 1]
set cdf_file [lindex $argv 0]
set sim_mode [lindex $argv 2]

#Intialize simulator
set ns [new Simulator]

#set queue type based on whether pfabric or tcp
set queue_type "DropTail"
if {$sim_mode == "p"} {
    set queue_type "PFabricQueue"
   #set queue_type "DropTail"
}
#set queue size to 150 if tcp run or 15 if pfabric run
set queue_size 150
if {$sim_mode == "p"} {
  set queue_size 15
}

proc finish {} {

  global ns tf
  exit 0
}
#INITIALIZATION COMPLETE ###################
#
#
#SETUP TOPOLOGY ################################
#create spine
for {set i 0} {$i < 4} {incr i} {
  set n($i) [$ns node]
  $n($i) color red
  $n($i) shape hexagon
}


#create pod nodes
for {set i 0} {$i < 4} {incr i} {
    #create 6 routers
    for {set j 0} {$j < 4} {incr j} {
	set p($i$j) [$ns node]   
	$p($i$j) color blue 
	$p($i$j) shape box
    }
}

#create edge nodes 
for {set i 0} {$i < 16} {incr i} {
    set e($i) [$ns node]
    $e($i) color green 
}

set curr_edge 0
#for each pod
for {set i 0} {$i < 4} {incr i} {
    #for lower pods indexes
    for {set j 2} {$j < 4} {incr j} {
	#over edges
	for {set k $curr_edge} {$k < [expr {$curr_edge + 4}]} {incr k} {
	    $ns duplex-link $p($i$j) $e($k) 1Gb .002ms $queue_type
	    $ns queue-limit $p($i$j) $e($k) $queue_size
        }

    }
    set curr_edge [expr {$curr_edge + 4}]

}
#links in pods
#for each pod
for {set i 0} {$i < 4} {incr i} {
  #for each upper router
  for {set j 0} {$j < 2} {incr j} {
    #connect to each of the lower routers
    for {set k 2} {$k < 4} {incr k} {
      $ns duplex-link $p($i$j) $p($i$k) 1Gb .002ms $queue_type
      $ns queue-limit $p($i$j) $p($i$k) $queue_size
    }

  }

}
set curr_pod 0
set r_0 0
set r_1 1
set r_2 2
#links from spines to pods
#for each spine
for {set i 0} {$i < 2} {incr i} {
  for {set j 0} {$j < 4} {incr j} {
    $ns duplex-link $p($j$r_0) $n($i) 1Gb .002ms $queue_type
    $ns queue-limit $p($j$r_0) $n($i) $queue_size
  }
}

for {set i 2} {$i < 4} {incr i} {
  for {set j 0} {$j < 4} {incr j} {
    $ns duplex-link $p($j$r_1) $n($i) 1Gb .002ms $queue_type
    $ns queue-limit $p($j$r_1) $n($i) $queue_size
  }
}

#TOPOLOGY CREATED #########################################
#pick random flow size
set search_path "./CDF/CDF_search.txt"
set mining_path "./CDF/CDF_datamining.txt"
set cdf [new RandomVariable/Empirical]
if {$cdf_file == "nw"} {
  puts "loading web_search CDF "
  $cdf loadCDF $search_path
}
if {$cdf_file == "dm"} {
    puts "loading data_mining CDF"
    $cdf loadCDF $mining_path
}



set fid 0
#read in generated times
      
set fp [open "times/$cdf_file/times_$workload.txt" "r"]
set file_data [read $fp]
close $fp
#close file
#parse data    
set data [split $file_data "\n"]
set conn_list [list]
puts "Establishing connections..."
foreach line $data {

    #pick a set of src/dest, open connection
    
    set portRNG [new RNG]
    set port [new RandomVariable/Uniform]
    $port set max_ 15
    $port set  min_ 0
    $port use-rng $portRNG

    set src [expr round([$port value])]
    set dest [expr round([$port value])]
    #loop unti src !== dest
    while {$dest == $src} {
        set dest [expr round([$port value])]

    }
    set flow_size [expr round([$cdf value])] 
   
	  
    set tcp [new Agent/TCP]
    if {$sim_mode == "p" } {
            set tcp [new Agent/TCP/Min]

     }
    $ns attach-agent $e($src) $tcp
    $tcp set packetSize_ 1460
    $tcp set fid_ $fid
    $tcp set start_time $line 
    $tcp set fsize_ $flow_size
    $tcp set cdf $cdf_file
    if { $sim_mode == "p" } {
        $tcp set windowInit_ 12
    }
    set fid [expr {$fid + 1}]
    set tcp_sink [new Agent/TCPSink]
    $ns attach-agent $e($dest) $tcp_sink
    $ns connect $tcp $tcp_sink	  
    set list [linsert $conn_list 0 $src$dest]
	
  
     
     set ftp [new Application/FTP]
     $ftp attach-agent $tcp
     $ftp set maxpkts_ $flow_size
     $ftp set maxpkts $flow_size
    
     $ftp set packetSize_ 1460 
     $ns at $line "$ftp produce $flow_size" 
}
puts "Connections established...Starting..."  
      
 
$ns at 600.0 "finish" 

$ns run
