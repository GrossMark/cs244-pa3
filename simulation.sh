#!/bin/bash
#run simulations 
#NOTE: MUST BE RUN UNDER SUDO
#NOTE: CHANGE PATH TO YOUR NS PATH, WON'T RUN OTHERWISE
ns="/home/ubuntu/ns-allinone-2.35/ns-2.35/ns"
#set up files for simulation
times_file="/times"
file_nw="$times_file/nw"
file_dm="$times_file/dm"
nam_file="/nam"
nam_nw="$nam_file/nw"
nam_dm="$nam_file/dm"
trace_file="/traces"
trace_dm="$trace_file/dm"
trace_nw="$trace_file/nw"
path=$PWD
parsed_fid="/parsed_flows"
outputs="/outputs"
pfabric="pfabric"
p_path="/$pfabric"
normal="tcp"
n_path="/$normal"
separation="------------------------------------------------------------------"
wel_message="Starting PFabric Simulation (2015): Azar Fazel & Mark Gross"
echo $wel_message
sudo_message="NOTE: Simulation must be run under sudo!!!"
echo $sudo_message
len_message="NOTE: simulation takes a long time to complete ~3 hours for
both websearch and datamining workloads"
echo $len_message

echo $separation
echo "Clearing previous simulation trial"
if [ -e $path$times_file ]
then
   rm -r $path$times_file
fi
if [ -e $path$nam_file ]
then
    rm -r $path$nam_file
fi
if [ -e $path$trace_file ]
then
    rm -r $path$trace_file
fi
if [ -e $path$parsed_fid ]
then
    rm -r $path$parsed_fid
fi
if [ -e $path$outputs ]
then
    rm -r $path$outputs
fi

echo "Opening new directories"
nw_p="/nw"
dm_p="/dm"
mkdir $path$times_file
mkdir $path$file_nw
mkdir $path$file_dm
mkdir $path$nam_file
mkdir $path$nam_nw
mkdir $path$nam_dm
mkdir $path$trace_file
mkdir $path$trace_nw
mkdir $path$trace_dm
mkdir $path$parsed_fid
mkdir $path$parsed_fid$nw_p
mkdir $path$parsed_fid$dm_p
mkdir $path$outputs
mkdir $path$outputs$p_path
mkdir $path$outputs$n_path
mkdir $path$outputs$p_path$nw_p
mkdir $path$outputs$p_path$dm_p
mkdir $path$outputs$n_path$nw_p
mkdir $path$outputs$n_path$dm_p
chmod 755 $path$outputs$p_path$nw_p
chmod 755 $path$outputs$p_path$dm_p
chmod 755 $path$outputs$n_path$nw_p
chmod 755 $path$outputs$n_path$dm_p

for load_times in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8; do
    newdir="/$load_times"
    mkdir $path$parsed_fid$nw_p$newdir
    mkdir $path$parsed_fid$dm_p$newdir
    chmod 755 $path$parsed_fid$nw_p$newdir
    chmod 755 $path$parsed_fid$dm_p$newdir
done
#generates start_times for simulations
#generates this number of start_times
num_times=10000
echo "Starting simulation...please be patient..."
echo $separation
for load_times in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8; do
  echo "Simulating workload: $load_times..."
  filename="/trace_$load_times.tr"
  lt="/$load_times"
  c="/nw"
  tmp="/tmp.txt"
  tmp2="/tmp2.txt"
  dir_path=$path$parsed_fid$c$lt
  $ns time_gen.tcl nw $load_times $num_times
  $ns time_gen.tcl dm $load_times $num_times
  echo "...tcp run..."
  $ns pfabric.tcl nw $load_times "n"
  #parse the tmp.txt
  python generate_fcts.py --dir $normal --wkld $load_times --cdf "nw"
  rm $path$tmp
  rm $path$tmp2
  echo $separation
  echo "...tcp run..."
  $ns pfabric.tcl dm $load_times "n"
  python generate_fcts.py --dir $normal --wkld $load_times --cdf "dm"
  rm $path$tmp
  rm $path$tmp2
  echo $separation
  echo "...pfabric run..."
  $ns pfabric.tcl nw $load_times "p"
  python generate_fcts.py --dir $pfabric --wkld $load_times --cdf "nw"
  rm $path$tmp
  rm $path$tmp2
  echo $separation
  echo "...pfabric run..."
  $ns pfabric.tcl dm $load_times "p"
  python generate_fcts.py --dir $pfabric --wkld $load_times --cdf "dm"
  rm $path$tmp
  rm $path$tmp2
  echo $separation
done
outf="/avg_fct.txt"
unoutf="/avg_unn_fct.txt"
plot_path_p_nw="$path$outputs$p_path$nw_p$outf"
plot_path_p_dm="$path$outputs$p_path$dm_p$outf"
plot_path_n_nw="$path$outputs$n_path$nw_p$outf"
plot_path_n_dm="$path$outputs$n_path$dm_p$outf"
plot_path_p_nw_un="$path$outputs$p_path$nw_p$unoutf"
plot_path_p_dm_un="$path$outputs$p_path$dm_p$unoutf"
plot_path_n_nw_un="$path$outputs$n_path$nw_p$unoutf"
plot_path_n_dm_un="$path$outputs$n_path$dm_p$unoutf"

Rscript plot_fct.R $plot_path_n_nw $plot_path_p_nw "nw" "n"
Rscript plot_fct.R $plot_path_n_dm $plot_path_p_dm "dm" "n"
Rscript plot_fct.R $plot_path_n_nw_un $plot_path_p_nw_un "nw" "un"
Rscript plot_fct.R $plot_path_n_dm_un $plot_path_p_dm_un "dm" "un"
echo "Experiment complete"
