import os
from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('--dir', '-d',
	            dest="dir",
		    type=str,
		    action='store',
		    required=True)
parser.add_argument('--wkld', '-wl',
	            dest="wkld",
		    type=str,
		    action='store',
		    required=True)
parser.add_argument('--cdf', '-c',
	            dest="cdf",
		    type=str,
		    action='store',
		    required=True)
args = parser.parse_args()
directory = args.dir
fcts = []
unfcts = []
file_path = "tmp.txt"
unnormal = "tmp2.txt"

#open both a normalized and unnormalized fct folder
with open(file_path, 'r') as f:
    lines = f.readlines()
with open(unnormal, 'r') as ff:
    lines_ = ff.readlines()
for l in lines:
    f = float(l)
    fcts.append(f)
for ll in lines_:
    fl = float(ll)
    unfcts.append(fl)

#calculate avgs for norm/unnorm versions
average_fct = sum(fcts)/float(len(fcts))
unaverage_fct = sum(unfcts)/float(len(unfcts))
print "cdf: " + args.cdf + " load: " + args.wkld + " " + str(average_fct)
print "unnormalized fcts: cdf: " + args.cdf + " load: " + args.wkld + " " + str(unaverage_fct)
filename = "outputs/" + args.dir + "/" + args.cdf + "/avg_fct.txt"
unfilename = "outputs/" + args.dir + "/" + args.cdf + "/avg_unn_fct.txt"
#output to output folder for norm/unnorm values
with open(filename, 'a') as f:
    output = args.wkld + " " + str(average_fct) + "\n"
    f.write(output)
with open(unfilename, 'a') as ff:
    output = args.wkld + " " + str(unaverage_fct) + "\n"
    ff.write(output)

   
